import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {

    private static final Map<String, Integer> SCORES = new HashMap<>();

    @Before
    public void setUp() {
        SCORES.put("Alice", 12);
        SCORES.put("Bob", 15);
        SCORES.put("Charlie", 11);
        SCORES.put("Delta", 15);
        SCORES.put("Emi", 15);
        SCORES.put("Foxtrot", 11);
    }

    @Test
    public void testExistingContent() {
        Map<Integer, List<String>> byScores = ScoreGrouping.groupByScores(SCORES);
        assertEquals(2, byScores.get(11).size());
        assertEquals(1, byScores.get(12).size());
        assertEquals(3, byScores.get(15).size());
    }

    @Test
    public void testContentNotExists() {
        Map<Integer, List<String>> byScores = ScoreGrouping.groupByScores(SCORES);
        assertNull(byScores.get(20));
    }

}
