package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Predicate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {

    private Applicant applicant;
    private Predicate<Applicant> creditCheck;
    private Predicate<Applicant> employmentCheck;
    private Predicate<Applicant> crimeCheck;
    Predicate<Applicant> qualifiedCheck;
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        applicant = new Applicant();
        creditCheck = theApplicant -> theApplicant.getCreditScore() > 600;
        employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
        crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();
        qualifiedCheck = Applicant::isCredible;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void creditCheckTest() {
        assertTrue(Applicant.evaluate(applicant, creditCheck));
        assertTrue(outContent.toString().contains("accepted"));
    }

    @Test
    public void employmentCheckTest() {
        assertTrue(Applicant.evaluate(applicant, employmentCheck));
    }

    @Test
    public void crimeCheckTest() {
        assertFalse(Applicant.evaluate(applicant, crimeCheck));
        assertTrue(outContent.toString().contains("rejected"));
    }

    @Test
    public void qualifiedCheckTest() {
        assertTrue(Applicant.evaluate(applicant, qualifiedCheck));
    }

    @Test
    public void multipleTest() {
        assertTrue(Applicant.evaluate(applicant, creditCheck));

        assertTrue(Applicant.evaluate(applicant,
                creditCheck.and(employmentCheck).and(qualifiedCheck)));

        assertFalse(Applicant.evaluate(applicant,
                crimeCheck.and(employmentCheck).and(qualifiedCheck)));

        assertFalse(Applicant.evaluate(applicant,
                crimeCheck.and(creditCheck).and(employmentCheck).and(qualifiedCheck)));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }
}
