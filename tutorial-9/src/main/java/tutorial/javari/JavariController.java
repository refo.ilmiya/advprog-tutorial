package tutorial.javari;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import tutorial.javari.animal.Animal;

import javax.validation.Valid;
import java.util.concurrent.atomic.AtomicLong;

public class JavariController {
    private static final String template = "Hello, %s! Welcome to JAVAri Park!";

    @Autowired
    private
    JavariRepository javariRepository;

    @GetMapping("/javari")
    public Animal getAnimal() {
        if(javariRepository.count() == 0) throw new ResourceNotFoundException();
        else return javariRepository.findAll().iterator().next();
    }

    @PostMapping("/javari")
    public Animal createAnimal(@Valid @RequestBody Animal animal) {
        return javariRepository.save(animal);
    }

    @GetMapping("/javari/{id}")
    public Animal getAnimalById(@PathVariable(value = "id") Long animalId) {
        return javariRepository.findById(animalId)
                .orElseThrow(() -> new ResourceNotFoundException());
    }

    @DeleteMapping("/javari/{id}")
    public Animal deleteNode(@PathVariable(value = "id") Long animalId) {
        Animal animal = javariRepository.findById(animalId)
                .orElseThrow(() -> new ResourceNotFoundException());

        javariRepository.delete(animal);

        return animal;
    }

}
