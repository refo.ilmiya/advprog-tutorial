package matrix;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static String genericMatrixPath = "../plainTextDirectory/input/matrixProblem";
    private static String pathFileMatrix1 = genericMatrixPath + "A/matrixProblemSet1.txt";

    private static String pathFileMatrix2 = genericMatrixPath + "A/matrixProblemSet2.txt";

    public static void main(String[] args) throws
            IOException, InvalidMatrixSizeForMultiplicationException {

        //Convert into array
        double[][] firstMatrix = convertInputFileToMatrix(pathFileMatrix1);
        double[][] secondMatrix = convertInputFileToMatrix(pathFileMatrix2);


        long totalMilisCalc = System.currentTimeMillis();
        //Example usage of basic multiplication algorithm.
        double[][] multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);
        totalMilisCalc = System.currentTimeMillis() - totalMilisCalc;
        System.out.println("Basic Calculation Complete in " + totalMilisCalc + " milisecond");


        //Example usage of strassen multiplication algorithm.
        totalMilisCalc = System.currentTimeMillis();
        double[][] strassenMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);
        totalMilisCalc = System.currentTimeMillis() - totalMilisCalc;
        System.out.println("Strassen Calculation Complete in " + totalMilisCalc + " milisecond");



    }

    /**
     * Converting a file input into an 2 dimensional array of double that represent a matrix.
     * @param pathFile is a path to file input.
     * @return 2 dimensional array of double representing matrix.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static double[][] convertInputFileToMatrix(String pathFile)
            throws IOException {
        File matrixFile = new File(pathFile);
        // read in the data
        ArrayList<ArrayList<Double>> a = new ArrayList<ArrayList<Double>>();
        Scanner input = new Scanner(matrixFile);
        while (input.hasNextLine()) {
            Scanner colReader = new Scanner(input.nextLine());
            ArrayList<Double> col = new ArrayList<Double>();
            while (colReader.hasNextInt()) {
                col.add(colReader.nextDouble());
            }
            a.add(col);
        }

        int row = a.size();
        int col = a.get(0).size();
        double[][] arr = new double[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                arr[i][j] = a.get(i).get(j);
            }
        }


        return arr;
    }

    /**
     * Converting a row of sequence of double into an array.
     * @param currentLine sequence of double from input representing a row from matrix.
     * @return array of double representing a row from matrix.
     */
    private static double[] sequenceIntoArray(String currentLine) {
        String[] arrInput = currentLine.split(" ");
        double[] arrInteger = new double[arrInput.length];
        for (int index = 0; index < arrInput.length; index++) {
            arrInteger[index] = Double.parseDouble(arrInput[index]);
        }
        return arrInteger;
    }
}
