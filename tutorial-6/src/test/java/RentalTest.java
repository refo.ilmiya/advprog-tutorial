import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {

    private Movie movie;
    private Rental rent;


    @Before
    public void setup() {
        movie = new Movie("How To Train Your Dragon", Movie.CHILDREN);
        rent = new Rental(movie, 4);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(4, rent.getDaysRented());
    }

    @Test
    public void getCharge() {
        assertEquals(3.0, rent.getCharge(), 0.00001);
    }
}