import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Rental rent;

    @Before
    public void setup() {
        customer = new Customer("Refo");
        movie = new Movie("How To Train Your Dragon", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }


    @Test
    public void getName() {
        assertEquals("Refo", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);

        movie = new Movie("How To Train Your Dragon 2", Movie.NEW_RELEASE);
        rent = new Rental(movie, 3);
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("Amount owed is 12.5"));
        assertTrue(result.contains("3 frequent renter points"));
    }

    @Test
    public void htmlstatementTest() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<EM>Refo</EM>"));
        assertTrue(result.contains("How To Train Your Dragon"));
        assertTrue(result.contains("<EM>3.5</EM>"));
        assertTrue(result.contains("<EM>1</EM>"));
    }
}