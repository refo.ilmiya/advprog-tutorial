import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {

    private Movie movie;

    @Before
    public void setup() {
        movie = new Movie("How To Train Your Dragon", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("How To Train Your Dragon", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equalsTest() {
        assertTrue(movie.equals(movie));
        assertFalse(movie == null);
        assertFalse(movie.equals(new Movie("How To Train Your Dragon 2", Movie.REGULAR)));

    }

    @Test
    public void getHashCode() {
        assertEquals(1551395342, movie.hashCode());
    }
}