import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        StringBuilder result = new StringBuilder("Rental Record for " + getName() + "\n");

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            // Show figures for this rental
            result.append("\t").append(each.getMovie().getTitle()).append("\t")
                    .append(String.valueOf(each.getCharge())).append("\n");
        }

        // Add footer lines
        result.append("Amount owed is ").append(String.valueOf(getTotalCharge())).append("\n");
        result.append("You earned ").append(String.valueOf(getTotalFrequentRenterPoints()))
                .append(" frequent renter points");

        return result.toString();
    }

    private double getTotalCharge() {
        double result = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += each.getCharge();
        }
        return result;
    }

    private int getTotalFrequentRenterPoints() {
        int result = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += each.getFrequentRenterPoints();
        }
        return result;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        StringBuilder result = new StringBuilder("<H1>Rentals for <EM>" + getName()
                + "</EM></H1><P>\n");
        while (iterator.hasNext()) {
            Rental each = (Rental) iterator.next();
            result.append(each.getMovie().getTitle()).append(": ")
                    .append(String.valueOf(each.getCharge())).append("<BR>\n");
        }
        result.append("<P>You owe <EM>").append(String.valueOf(getTotalCharge()))
                .append("</EM><P>\n");
        result.append("On this rental you earned <EM>")
                .append(String.valueOf(getTotalFrequentRenterPoints()))
                .append("</EM> frequent renter points<P>");
        return result.toString();
    }

}