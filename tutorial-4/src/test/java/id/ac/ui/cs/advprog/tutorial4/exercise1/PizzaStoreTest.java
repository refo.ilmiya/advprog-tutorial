package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

public class PizzaStoreTest {

    PizzaStore toko;

    @Before
    public void setUp() throws Exception {
        toko = new DepokPizzaStore();
    }

    @Test
    public void testorder() {
        toko.orderPizza("cheese");
        toko.orderPizza("veggie");
        toko.orderPizza("clam");
    }
}
