package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.RefoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.RefoClam;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.RefoDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.RefoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RefoVeg;
import org.junit.Before;
import org.junit.Test;

public class PizzaTest {
    private Pizza pizzatest;
    @Before
    public void setup() throws Exception {
        DepokPizzaIngredientFactory ingredient = new DepokPizzaIngredientFactory();
        pizzatest = new Pizza() {
            @Override
            public void prepare() {
                dough = ingredient.createDough();
                sauce = ingredient.createSauce();
                cheese = ingredient.createCheese();
                veggies = ingredient.createVeggies();
                clam = ingredient.createClam();
            }
        };
    }

    @Test
    public void toStringtest() {
        pizzatest.prepare();
        String res = pizzatest.toString();
        assertEquals("---- null ----\n" +
                "Refo Dough\n" +
                "Refo Sauce\n" +
                "Refo Cheese\n" +
                "Refo Veg, Black Olives, Eggplant, Spinach\n" +
                "Refo Clams from Bogor with love\n", res);

    }
}
