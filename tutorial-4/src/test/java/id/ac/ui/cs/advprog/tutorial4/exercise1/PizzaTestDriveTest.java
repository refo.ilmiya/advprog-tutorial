package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;


public class PizzaTestDriveTest {
    private PizzaTestDrive testDrive;

    @Before
    public void setup() throws Exception {
        testDrive = new PizzaTestDrive();
    }

    @Test
    public void maintest() {
        String[] input = {"test"};
        PizzaTestDrive.main(input);
    }
}
