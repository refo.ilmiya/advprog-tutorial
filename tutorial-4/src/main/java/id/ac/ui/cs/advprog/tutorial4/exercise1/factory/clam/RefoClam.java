package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RefoClam implements Clams {

    public String toString() {
        return "Refo Clams from Bogor with love";
    }

}
