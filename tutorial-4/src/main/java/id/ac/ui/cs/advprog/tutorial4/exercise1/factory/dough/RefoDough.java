package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class RefoDough implements Dough {
    public String toString() {
        return "Refo Dough";
    }

}
