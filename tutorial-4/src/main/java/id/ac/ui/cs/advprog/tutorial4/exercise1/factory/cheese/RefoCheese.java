package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class RefoCheese implements Cheese {

    public String toString() {
        return "Refo Cheese";
    }

}
