package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        // Create a new Pizza Store franchise at Depok
        PizzaStore dpStore = new DepokPizzaStore();
        Pizza pizza1 = dpStore.orderPizza("cheese");
        System.out.println("Refo ordered a " + pizza1 + "\n");

        pizza1 = dpStore.orderPizza("clam");
        System.out.println("Refo ordered a " + pizza1 + "\n");

        pizza1 = dpStore.orderPizza("veggie");
        System.out.println("Refo ordered a " + pizza1 + "\n");
    }
}
