package game;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by billy on 9/27/16.
 * Edited by hafiyyan94 on 4/10/18
 */

public class Main {

    public static void main(String[] args) {
        // write your code here
        QuestionGenerator questions = new QuestionGenerator();
        questions.start();
    }
}
