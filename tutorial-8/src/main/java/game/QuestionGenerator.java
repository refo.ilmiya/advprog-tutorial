package game;

import java.util.Random;
import java.util.Scanner;

public class QuestionGenerator implements Runnable {

    private Thread thread;
    private static final int TOTAL_QUEST = 10;
    private static final int QUEST_TYPE_ADD = 0;
    private static final int QUEST_TYPE_SUBSTR = 1;
    private static final int QUEST_TYPE_MULTIPL = 2;
    private static final int QUEST_TYPE_DIVS = 3;
    private double totalscore;
    private int totaltime;

    QuestionGenerator() {
        this.totalscore = 0;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        String startNewQuestsIpt;
        Scanner scanner = new Scanner(System.in);

        do {
            // initialize value
            startNewQuestsIpt = "";

            // Asking for asnwering question threshold time
            System.out.print("How much time do you need "
                    + "to answer each question? (In second) ");
            String rawInput = scanner.nextLine();
            int thresholdTime = rawInput.isEmpty() ? 20 : Integer.parseInt(rawInput);
            Timer timeref = new Timer();
            GameCheck gameCheck = new GameCheck();
            //Scanner scanner = new Scanner(System.in);
            for (int questNo = 1; questNo <= TOTAL_QUEST; questNo++) {
                TimerDecrement score = new TimerDecrement(gameCheck);
                TimerIncrement timerquest = new TimerIncrement(timeref);
                timerquest.start();
                score.start();
                System.out.print(questNo + ") ");
                Random rand = new Random();
                Fraction firstPosFrac = new Fraction(rand.nextInt(40) - 20,
                        rand.nextInt(40) - 20);
                Fraction secondPosFrac = new Fraction(rand.nextInt(40) - 20,
                        rand.nextInt(40) - 20);
                Fraction expectedAnswer;

                switch (rand.nextInt(3)) {
                    case QUEST_TYPE_ADD:
                        System.out.print(firstPosFrac.toString() + "  +  "
                                + secondPosFrac.toString() + "  =  ");
                        expectedAnswer = firstPosFrac.getAddition(secondPosFrac);
                        break;
                    case QUEST_TYPE_SUBSTR:
                        System.out.print(firstPosFrac.toString() + "  -  "
                                + secondPosFrac.toString() + "  =  ");
                        expectedAnswer = firstPosFrac.getSubstraction(secondPosFrac);
                        break;
                    case QUEST_TYPE_MULTIPL:
                        System.out.print(firstPosFrac.toString() + "  *  "
                                + secondPosFrac.toString() + "  =  ");
                        expectedAnswer = firstPosFrac.getMultiplication(secondPosFrac);
                        break;
                    case QUEST_TYPE_DIVS:
                        System.out.print(firstPosFrac.toString() + "  :  "
                                + secondPosFrac.toString() + "  =  ");
                        expectedAnswer = firstPosFrac.getDivision(secondPosFrac);
                        break;
                    default:
                        System.out.println("Oooops!");
                        expectedAnswer = new Fraction();
                }
                // Asking for question
                // And capture before and after the time in milis
                long totalMilis = System.currentTimeMillis();
                String rawAns = scanner.nextLine();
                totalMilis = System.currentTimeMillis() - totalMilis;
                setTotaltime(getTotaltime() + timeref.value());
                // Process user answer
                Fraction userAnswer;
                if (rawAns.contains("/")) {
                    String[] ans = rawAns.split("/");
                    userAnswer = new Fraction(Integer.parseInt(ans[0]),
                            Integer.parseInt(ans[1]));
                } else {
                    userAnswer = new Fraction(Integer.parseInt(rawAns));
                }
                timerquest.stop();
                score.stop();

                // Check answer
                double temp = 0;
                if (expectedAnswer.isEqual(userAnswer)) {
                    if (totalMilis / 1000 <= thresholdTime) {
                        temp = gameCheck.value() * 0.1;

                    } else {
                        temp = gameCheck.value() * 0.05;
                    }
                }
                setTotalScore(gameCheck.value() + temp);

                try {
                    thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // Print the result
            System.out.println("\n=========Result==========");


            int totalPoint = (int) getTotalscore();
            System.out.println("\nTotal point acquired : " + totalPoint);
            int totalTime = getTotaltime();
            System.out.println("\nTotal time to finish : " + totalTime);

            System.out.println("\n");

            // Asking if user want to start a new questions
            // if the respond is not what we want, ask it again and again
            while (!startNewQuestsIpt.equalsIgnoreCase("y")
                    && !startNewQuestsIpt.equalsIgnoreCase("n")) {
                System.out.println("Restart the quiz? [y/n]");
                startNewQuestsIpt = scanner.nextLine();
            }
            System.out.println("\n\n\n\n\n\n");
        } while (startNewQuestsIpt.equalsIgnoreCase("y"));
    }

    private void setTotalScore(double num) {
        this.totalscore = num;
    }

    public double getTotalscore() {
        return this.totalscore;
    }

    public int getTotaltime() {
        return totaltime;
    }

    private void setTotaltime(int totaltime) {
        this.totaltime = totaltime;
    }

    public void stop() {
        thread.interrupt();
    }
}
