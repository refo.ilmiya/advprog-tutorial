package game;

public class GameCheck {
    private double counter = 100;

    public synchronized void increment() {
        counter++;
    }

    public synchronized void setCounter(double num) {
        counter = num;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized double value() {
        return counter;
    }
}
