package game;

public class TimerIncrement implements Runnable {

    private Timer gameReference;
    private Thread thread;

    public int getThisquestiontime() {
        return thisquestiontime;
    }

    private void setThisquestiontime(int thisquestiontime) {
        this.thisquestiontime = thisquestiontime;
    }

    private int thisquestiontime = 0;

    public TimerIncrement(Timer gameReference) {
        this.gameReference = gameReference;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                gameReference.increment();
            }
        } catch (InterruptedException e) {
            setThisquestiontime(gameReference.value());
            System.out.println("Time for this question: " + gameReference.value());
            gameReference.setCounter(0);
        }
    }

    public void stop() {
        thread.interrupt();
    }

}
