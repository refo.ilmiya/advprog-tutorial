package game;

public class Timer {
    private int counter = 0;

    public synchronized void increment() {
        counter++;
    }

    public synchronized void setCounter(int num) {
        counter = num;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized int value() {
        return counter;
    }
}
