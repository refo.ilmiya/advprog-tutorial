package game;

public class TimerDecrement implements Runnable {

    private GameCheck gameReference;
    private Thread thread;

    public TimerDecrement(GameCheck gameReference) {
        this.gameReference = gameReference;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                gameReference.decrement();
            }
        } catch (InterruptedException e) {
            System.out.println("Score: " + gameReference.value());
        }
    }

    public void stop() {
        thread.interrupt();
    }
}
