package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class CompositeMainClass {
    public static void main(String[] args) {
        Company refoSoftware = new Company();

        Ceo gabeN = new Ceo("Gabe Newell", 500000.00);
        refoSoftware.addEmployee(gabeN);

        Cto revan = new Cto("Darth Revan", 320000.00);
        refoSoftware.addEmployee(revan);

        BackendProgrammer freeman = new BackendProgrammer("Gordon Freeman", 94000.00);
        refoSoftware.addEmployee(freeman);

        BackendProgrammer ifan = new BackendProgrammer("Ifan ben-Medz", 200000.00);
        refoSoftware.addEmployee(ifan);

        FrontendProgrammer ela = new FrontendProgrammer("Elżbieta Ela Bosak",66000.00);
        refoSoftware.addEmployee(ela);

        FrontendProgrammer garrus = new FrontendProgrammer("Garrus Vakarian", 130000.00);
        refoSoftware.addEmployee(garrus);

        UiUxDesigner jensen = new UiUxDesigner("Adam Jensen", 177000.00);
        refoSoftware.addEmployee(jensen);

        NetworkExpert courier = new NetworkExpert("Courier Six", 83000.00);
        refoSoftware.addEmployee(courier);

        SecurityExpert geralt = new SecurityExpert("Geralt of Rivia", 70000.00);
        refoSoftware.addEmployee(geralt);

        System.out.println("Company's Employee: ");
        refoSoftware.getAllEmployees().stream()
                .forEach(e -> System.out.println(e.getName() + " as " + e.getRole()));

        System.out.println("Net salaries: " + refoSoftware.getNetSalaries());
    }

}
