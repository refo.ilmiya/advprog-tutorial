package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMainClass {
    public static void main(String[] args) {
        Food minimac = BreadProducer.THICK_BUN.createBreadToBeFilled();
        minimac = FillingDecorator.BEEF_MEAT.addFillingToBread(minimac);
        minimac = FillingDecorator.LETTUCE.addFillingToBread(minimac);
        minimac = FillingDecorator.TOMATO.addFillingToBread(minimac);
        minimac = FillingDecorator.CHILI_SAUCE.addFillingToBread(minimac);
        minimac = FillingDecorator.CUCUMBER.addFillingToBread(minimac);

        System.out.println(minimac.getDescription() + " costs " + minimac.cost());

    }

}
