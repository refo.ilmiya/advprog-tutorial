package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CuriculumVitaeController {

    @GetMapping("/curiculumvitae")
    public String greeting(@RequestParam(name = "visitor", required = false)
                                       String visitor, Model model) {
        if (visitor == null || visitor.equals("")) {
            model.addAttribute("visitor", "This is my CV");
        } else {
            model.addAttribute("visitor", visitor + ", I hope you interested to hire me");
        }
        String name = "Refo Ilmiya Akbar";
        String birthdate = "October 31 1998";
        String birthplace = "Bogor";
        String address = "15 Bhisma Raya St. Bumi Indraprasta 2, Bogor";
        String[] education = {
                "Computer Science, University of Indonesia (2016-present)",
                "SMAN 1 Bogor (2013-2016)",
                "SMPIT Ummul Quro Bogor (2010-2013)",
                "SDIT Ummul Quro Bogor (2004-2010)"};
        String[] desc = {"I'm an average college student. My hobby is studying and reading books.",
                "In my free time, I'd love to browse the internet to expand my knowledge.",
                "Currently, I'm searching for an internship to fill my holiday this semester."};
        model.addAttribute("name", name);
        model.addAttribute("birthdate", birthdate);
        model.addAttribute("birthplace", birthplace);
        model.addAttribute("address", address);
        model.addAttribute("education", education);
        model.addAttribute("description", desc);
        return "curiculumvitae";
    }

}
